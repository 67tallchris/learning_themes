<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WordPress Training Wheels</title>

<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
</head>

<body>

	<div id="container">
		
		<div id="header"><!-- Header Begins Here -->
			<h1><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
			<h2><?php bloginfo('description'); ?></h2>
		</div>
		
		<div id="menu">
        
			<ul>
            	<li><a href="index.html">Home</a></li>
				<li><a href="#">About</a></li>
                <li><a href="#">More Info</a></li>
                <li><a href="#">Contact</a></li>
			</ul>

		</div>
		<div id="content">
			
			<div id="sidebar" class="left"><!-- Left Sidebar Begins Here -->
				<h4>Sidebar Header</h4>
			</div>
			
			<div id="middle-column"><!-- Main Content Begins Here -->
				<h3>Training Wheels Lesson 1</h3>
                <p><img src="<?php bloginfo('template_directory'); ?>/images/training-wheels.jpg" width="426" height="142" alt="Training Wheels" /></p>
			</div>
			
			<div id="sidebar" class="right"><!-- Right Sidebar Begins Here -->
				<h4>Sidebar Header</h4>
			</div>
			
			<div style="clear:both;"></div>
			
		</div><!-- Content Ends Here -->
		
		<div id="footer"><!-- Footer Content Begins Here -->
			<p>&copy; Wordpress Training Wheels, by wpbedouine</p>
		</div>
	
	</div>
	
</body>

</html>
